Configurando seu projeto
========================

Este arquivo contém informações gerais sobre como configurar seu projeto e entender a estrutura básica de arquivos. Alguns conceitos do *React* são citados aqui, mas os detalhes estão em outro arquivo.

Configurando seu smartphone
---------------------------

Para desenvolver utilizando um smartphone físico (*i.e.* o que você carrega no bolso), existem três requerimentos:

1. Seu smartphone deve ter o *USB Debugging* ligado (pesquise no google como liberar para seu aparelho);

2. A versão do Android deve ser >= 4.1;

3. Você deve ter um cabo USB que suporte transmissão de dados com seu smartphone.

Quando tiver certeza de que os dois requerimentos estão ok, prossiga com a leitura.

___

Guia de sobrevivência
---------------------

Bom, o primeiro passo é aprender a criar e se virar dentro de um projeto.

Para criar um projeto, utilize o comando `react-native init <meu_projeto>`, onde `<meu_projeto>` é o nome do seu projeto (que vai ser o nome da pasta e de várias coisas dentro dela). O padrão é utilizar o nome em PascalCase.

Digamos que o nome do seu projeto é `TestProject`. Ao abrir um terminal na pasta, você terá os seguintes comandos disponíveis:

```bash
# Inicia o servidor da aplicação React Native (trava o terminal)
npm start

# Compila seu projeto e joga ele no seu smartphone
react-native run-android
```

Você **deve manter um terminal aberto com o servidor (*i.e.* executando o comando `npm start`)**. O servidor deve estar rodando antes de executar o comando `react-native run-android`.

Pra começar a desenvolver, conecte seu smartphone ao computador, deixe o servidor rodando em um terminal, e num outro terminal, execute o comando `react-native run-android`.

Se for a primeira vez que executa, deve demorar um pouco mais para o comando terminar.

E se for a primeira vez que seu smartphone é conectado ao computador para desenvolvimento, fique atento: assim que o comando estiver próximo de encerrar, um prompt aparecerá na tela do seu smartphone perguntando se você confia naquele computador. Selecione a caixa para sempre confiar e confirme o prompt. Se você não completar esses passos à tempo, um erro de **`no connected devices`** vai ocorrer. Nesse caso, basta re-executar o comando.

Após executar o comando, seu smartphone deve abrir automagicamente o seu aplicativo. Se for um projeto novo, a tela deverá ser semelhante à essa:

<img src="img/react-native-initial-screen.jpg" title="Tela inicial" alt="Tela inicial de um novo aplicativo React Native" width="480" height="720">

Como nosso smartphone é físico, você pode chacoalhá-lo para ver o menu de desenvolvimento. No início é bem legal, mas depois de 30 minutos você já vai estar puto de ficar chacoalhando ele. Não se preocupe, podemos resolver isso.

Aliás, o menu de desenvolvimento é parecido (ou idêntico) com isso:

<img src="img/react-native-developer-menu.jpg" alt="Menu de desenvolvimento de um aplicativo React Native" title="Menu de desenvolvimento" width="480" height="720">

Não vou abordar o que cada comando do menu faz, mas sugiro fortemente habilitar o **Hot Reloading**, que basicamente atualiza sua aplicação automaticamente quando você salva/altera um arquivo.

Se algum erro ocorrer, visite a [seção de erros comuns](#erros-comuns) e verifique se seu erro consta lá. Senão, jogue no Google e/ou procure um coleguinha.

___

Entendendo a estrutura de um projeto React Native
-------------------------------------------------

Ao abrir a pasta do seu projeto, você vai se deparar com um monte de arquivos de cara:

![Pasta root de um projeto React Native](img/react-native-project-root-folder.png "Pasta root")

Tudo se inicia no arquivo `index.js`. Você dificilmente terá que mexer nesse arquivo.

Por padrão, o único objetivo desse arquivo é renderizar seu componente pai, ou **componente raíz**, que é o `App`. Se o seu projeto foi recém-criado, compare a tela inicial que abriu em seu smartphone com o que está dentro de `App.js`.

O arquivo `app.json` serve para você colocar informações relevantes sobre o projeto (por padrão, ele só tem o nome do projeto).

Os demais arquivos da pasta raíz são arquivos de configuração (`.watchmanconfig`, `babel.config.js`, `package.json`, `.gitignore`, `.gitattributes`, `.flowconfig`, `.buckconfig`). Não se preocupe muito com eles.

As pastas `android/` e `ios/` contém toda a estrutura necessária para você conseguir compilar seu projeto para ser executado no smartphone. Você geralmente mexe nelas apenas para adicionar algum plugin ou para alterar os ícones (*favicons*), mas para isso tem tutorial em todo lugar.

___

Dicas para estudar
------------------

O componente raíz da aplicação (`App`) é geralmente usado para inicializar todos os extras da sua aplicação React, como o *Redux* e o *Router*.

A maioria dos projetos divide a aplicação em duas pastas principais: uma para guardar componentes genéricos (quase sempre chamada de `components/`), como Containers, Cards, Backgrounds; e outra para guardar as "páginas" (nomeada `pages/`, `screens/`, `routes/`, etc).

A diferenciação básica é que "páginas são feitas de componentes, e um componente só aparece na aplicação através de uma página".

Se você está lidando com um projeto já em desenvolvimento, explore essas pastas e procure entender como elas interagem entre si. Isso deve deixar as coisas bem mais fáceis.

___

Dicas para desenvolver
----------------------

Depois de passar uma semana brincando com React Native, você vai desistir de ficar chacoalhando seu smartphone e vai começar a jogar ele na parede.

Antes disso acontecer, saiba que existem algumas saídas via terminal:

```bash
# Reload manual
adb shell input text "RR"

# Abrir o menu de desenvolvimento
adb shell input keyevent 82
```

O recomendado é criar `aliases` como `reload-app` e `open-app-menu` para não precisar digitar o comando todo.

___

Erros comuns
------------

### Could not connect to development server

Isso acontece porque seu smartphone "esqueceu" a porta do seu servidor. Para consertar isso, é só reconfigurar a porta `tcp:8081` do seu smartphone para se comunicar com a porta `tcp:8081` do computador, com este comando:

```bash
adb reverse tcp:8081 tcp:8081
```

### No connected devices

Esse erro pode ocorrer por vários motivos:

* Seu cabo está com mal contato;
* Seu smartphone não está com *USB Debugging* ligado;
* Seu smartphone não está no modo de transferência de dados (em alguns modelos, esse não é o comportamento padrão).

Depois de verificar que os itens acima estão ok, re-execute o comando `react-native run-android` e tente novamente.

### Erro `ENOSPC` ao executar `npm start`

Esse erro ocorre porque o Linux possui um limite relativamente baixo para "quantos arquivos seu programa pode ficar de olho". O servidor da sua aplicação precisa manter milhares de gatilhos (`file watch`) sobre os arquivos do projeto, motivo que ocasiona o rompimento ilegal desse limite.

Para consertá-lo, pare seu servidor e execute o comando abaixo:

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```

Esse comando simplesmente aumenta o limite para 524288 arquivos, que é bem grande. Ele deve ser executado uma única vez.
