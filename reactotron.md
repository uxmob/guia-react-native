Instalando e configurando o [Reactotron](https://github.com/infinitered/reactotron)
===================================================================================

Uma coisa que você com certeza sentirá falta ao programar sua aplicação React Native é a falta de um *output*. Vivemos de `printf()` em C e `console.log()` em web para depurar nosso código, então nossa mente já vem configurada para aceitar e trabalhar com esse recurso.

Logo, precisamos de um meio decente de ver o que está acontecendo com nossa aplicação. Por isso, vamos utilizar uma ferramenta extremamente útil e *clean*, que é o [Reactotron](https://github.com/infinitered/reactotron).

Reactotron, para nós, será um programa Desktop. Você o integra com a aplicação através de um pacote instalável via *npm* e baixa também a aplicação Desktop, mas depois de integrar você não precisa mais fazer nada em código além de usar o comando de *log*.

O [guia de instalação oficial está aqui](https://github.com/infinitered/reactotron/blob/master/docs/quick-start-react-native.md). Meu objetivo é só transcrever.

Baixe a aplicação Desktop
-------------------------

Vá para a [página de *Releases*](https://github.com/infinitered/reactotron/releases) e baixe a versão mais recente. Se você é um usuário Linux, recomendo baixar o arquivo `.deb` e instalar com o comando `sudo dpkg -i [nome-do-pacote]`.

Essa é a cara do aplicativo:

![Página inicial do Reactotron](img/reactotron-main-screen.png "Página inicial")

Integrando com nossa aplicação
------------------------------

Os passos são simples: Instale o pacote *npm* com `npm install --save-dev reactotron-react-native`, crie um arquivo separado na mesma pasta em que seu `index.js` chamado `ReactotronConfig.js` (ou outro nome, se não gostar desse) e adicione o seguinte:

```js
import Reactotron from 'reactotron-react-native';

Reactotron
  .configure({ host: 'ip-local-da-sua-maquina' }) // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .connect() // let's connect!

console.tron = __DEV__ ? Reactotron : () => {};
```

Repare que precisamos do IP local do servidor (que é `192.168.0.x`, sendo x um número de 2 a 254), que vai variar de pessoa para pessoa, e de rede para rede. Dá pra ver que isso vai dar problema se você tiver dois ou mais desenvolvedores no projeto.

Mas em essência, isso deve funcionar para a configuração. Mas para evitar problemas, vamos bolar algo mais *fancy*: crie um arquivo no mesmo nível que o `index.js` chamado `env.example.js` (vamos copiar o *setup* que o Laravel faz) e coloque **exatamente** o seguinte nele:

```js
export default {
  ip: "ip-local-da-sua-maquina"
}
```

Agora, duplique esse arquivo e chame a duplicata de `env.js`, e nele altere a string `"ip-local-da-sua-maquina"` para o IP local da sua máquina, e adicione esse IP de volta no seu `ReactotronConfig.js`:

```js
import Reactotron from 'reactotron-react-native';
import env from "./env";

Reactotron
  .configure({
    host: env.ip
  }) // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .connect() // let's connect!

console.tron = __DEV__ ? Reactotron : () => {};
```

Por último, abra o arquivo `.gitignore` que está no mesmo nível que o `index.js` e adicione seu `env.js` para a lista de arquivos ignorados (é só colocar `env.js` em uma linha).

E em seu arquivo `index.js`, adicione este `import` antes da chamada ao `AppRegistry`:

```js
if (__DEV__) {
    import("./ReactotronConfig");
}
```

A variável `__DEV__` é uma global criada pelo servidor React Native. Ela contém `true` quando você está em modo de desenvolvimento (vulgo `npm start`). Fazemos isso porque o Reactotron é pesado, e ele é totalmente inútil na versão de produção, porque ninguém vai ficar conectando o celular com *USB Debugging* ligado no computador pra ver se deixamos ele ativado. :)

Agora abra o Reactotron (Desktop), dê um *reload* na sua aplicação, e uma mensagem "CONNECTION" deverá aparecer na *timeline*:

![Reactotron conectado](img/reactotron-connected.png "Reactotron Connectado")

Usando o Reactotron
-------------------

Leia as [dicas da documentação oficial](https://github.com/infinitered/reactotron/blob/master/docs/tips.md), para começar.

O método mais simples e mais útil é o `console.tron.log()`, que é o `console.log()` que manda a mensagem direto para a *timeline* do Reactotron. Nesse *log* você pode imprimir o que quiser (objetos, arrays, strings, etc).

Se quiser fazer um teste rápido, abra seu `App.js` e adicione o comando `console.tron.log("HELLO WORLD")` na primeira linha do método `render()`. Depois, é só recarregar a sua aplicação e ver se a mensagem aparece:

![Teste de primeira mensagem com Reactotron](img/reactotron-test-message.png "Primeira mensagem de teste")

Só vai faltar integrá-lo com o *Redux*, que vamos fazer depois de aprender sobre ele. :)
